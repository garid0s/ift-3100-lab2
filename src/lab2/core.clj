(ns lab2.core
  (:require [clojure.java.io :as io]
            [quil.core :as q]
            [quil.middleware :as m]))

(def output-file "output.png")
(def width 800)
(def height 800)
(def all-covers-path (->> "covers" io/file file-seq rest (map str)))

(defn setup
  []
  {:images (map q/load-image all-covers-path)})

(defn draw-state
  [state]
  (q/background 0 0 0)
  (let [images (:images state)]
    (q/image (nth images 0) 0 0 400 400)
    (q/image (nth images 1) 400 0 400 400)
    (q/image (nth images 2) 0 400 400 400)
    (q/image (nth images 3) 400 400 400 400)))

(defn save-image
  [state event]
  (q/save output-file)
  state)

(q/defsketch lab2
  :title "Labo 2"
  :size [width height]
  :setup setup
  :key-pressed save-image
  :draw draw-state
  :middleware [m/fun-mode])
